Repository that contains all the proto files used in the Gen.2 software.

To clone
```bash
git clone --recurse-submodules https://gitlab.com/rcbenchmark/rcbenchmark-proto_files
```


A docker image and a script are also provided to generate files from them.
To update them run the command inside this repo:
```bash
docker run -v $(pwd):/mnt befuhro/rcb-proto-gen
```
or this one for windows 10 (repo must be cloned using WSL and not gitbash)
```bash
docker run -v ${pwd}:/mnt befuhro/rcb-proto-gen

```

To rebuild the Dockerfile
```bash
docker build -t rcb-proto-gen .

```


When files are generated, copy-paste the files that have changed to the clients (GUI and python interpreter).

The files inside javascript/communicator should go to src/grpc_client/communicator on the GUI, same for the core_manager.

All the files from python/communicator and python/core_manager should go to rcbenchmark/grpc on the python API.

And after update the go modules with the following command (the part after the @ must be the last commit id) inside the folder app on the core repository.
```bash
go get gitlab.com/rcbenchmark/rcbenchmark-proto_files@92270577
```