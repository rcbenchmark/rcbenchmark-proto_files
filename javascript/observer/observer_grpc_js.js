// GENERATED CODE -- DO NOT EDIT!

'use strict';
var observer_pb = require('./observer_pb.js');
var handshake_pb = require('./handshake_pb.js');
var parameter_pb = require('./parameter_pb.js');
var sensor_data_pb = require('./sensor_data_pb.js');

function serialize_data_SensorData(arg) {
  if (!(arg instanceof sensor_data_pb.SensorData)) {
    throw new Error('Expected argument of type data.SensorData');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_data_SensorData(buffer_arg) {
  return sensor_data_pb.SensorData.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_observeproto_BoardParameters(arg) {
  if (!(arg instanceof observer_pb.BoardParameters)) {
    throw new Error('Expected argument of type observeproto.BoardParameters');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_observeproto_BoardParameters(buffer_arg) {
  return observer_pb.BoardParameters.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_observeproto_Empty(arg) {
  if (!(arg instanceof observer_pb.Empty)) {
    throw new Error('Expected argument of type observeproto.Empty');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_observeproto_Empty(buffer_arg) {
  return observer_pb.Empty.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_observeproto_RefreshRate(arg) {
  if (!(arg instanceof observer_pb.RefreshRate)) {
    throw new Error('Expected argument of type observeproto.RefreshRate');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_observeproto_RefreshRate(buffer_arg) {
  return observer_pb.RefreshRate.deserializeBinary(new Uint8Array(buffer_arg));
}


var ObserverService = exports['observeproto.Observer'] = {
  // Method that streams data
  observeData: {
    path: '/observeproto.Observer/ObserveData',
    requestStream: true,
    responseStream: true,
    requestType: observer_pb.RefreshRate,
    responseType: sensor_data_pb.SensorData,
    requestSerialize: serialize_observeproto_RefreshRate,
    requestDeserialize: deserialize_observeproto_RefreshRate,
    responseSerialize: serialize_data_SensorData,
    responseDeserialize: deserialize_data_SensorData,
  },
  // Method that streams notifications
  observeBoards: {
    path: '/observeproto.Observer/ObserveBoards',
    requestStream: false,
    responseStream: true,
    requestType: observer_pb.Empty,
    responseType: observer_pb.BoardParameters,
    requestSerialize: serialize_observeproto_Empty,
    requestDeserialize: deserialize_observeproto_Empty,
    responseSerialize: serialize_observeproto_BoardParameters,
    responseDeserialize: deserialize_observeproto_BoardParameters,
  },
};

