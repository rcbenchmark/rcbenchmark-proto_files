# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

import logger_pb2 as logger__pb2


class LoggerStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.HandleLoggerInformation = channel.stream_stream(
        '/logger.Logger/HandleLoggerInformation',
        request_serializer=logger__pb2.LoggerInformation.SerializeToString,
        response_deserializer=logger__pb2.LoggerInformation.FromString,
        )
    self.GenerateCSV = channel.unary_unary(
        '/logger.Logger/GenerateCSV',
        request_serializer=logger__pb2.CSVInfo.SerializeToString,
        response_deserializer=logger__pb2.Empty.FromString,
        )
    self.TakeSample = channel.unary_unary(
        '/logger.Logger/TakeSample',
        request_serializer=logger__pb2.Empty.SerializeToString,
        response_deserializer=logger__pb2.Empty.FromString,
        )
    self.BenchmarksInformation = channel.stream_stream(
        '/logger.Logger/BenchmarksInformation',
        request_serializer=logger__pb2.Benchmarks.SerializeToString,
        response_deserializer=logger__pb2.Benchmarks.FromString,
        )
    self.DeleteBenchmark = channel.unary_unary(
        '/logger.Logger/DeleteBenchmark',
        request_serializer=logger__pb2.Benchmark.SerializeToString,
        response_deserializer=logger__pb2.Empty.FromString,
        )


class LoggerServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def HandleLoggerInformation(self, request_iterator, context):
    """Keep clients up to date with logger information
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GenerateCSV(self, request, context):
    """Generate CSV file inside the directory
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def TakeSample(self, request, context):
    """Take sample at t time
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def BenchmarksInformation(self, request_iterator, context):
    """Handle meta data of previous benchmarks
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def DeleteBenchmark(self, request, context):
    """Delete benchmark
    """
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_LoggerServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'HandleLoggerInformation': grpc.stream_stream_rpc_method_handler(
          servicer.HandleLoggerInformation,
          request_deserializer=logger__pb2.LoggerInformation.FromString,
          response_serializer=logger__pb2.LoggerInformation.SerializeToString,
      ),
      'GenerateCSV': grpc.unary_unary_rpc_method_handler(
          servicer.GenerateCSV,
          request_deserializer=logger__pb2.CSVInfo.FromString,
          response_serializer=logger__pb2.Empty.SerializeToString,
      ),
      'TakeSample': grpc.unary_unary_rpc_method_handler(
          servicer.TakeSample,
          request_deserializer=logger__pb2.Empty.FromString,
          response_serializer=logger__pb2.Empty.SerializeToString,
      ),
      'BenchmarksInformation': grpc.stream_stream_rpc_method_handler(
          servicer.BenchmarksInformation,
          request_deserializer=logger__pb2.Benchmarks.FromString,
          response_serializer=logger__pb2.Benchmarks.SerializeToString,
      ),
      'DeleteBenchmark': grpc.unary_unary_rpc_method_handler(
          servicer.DeleteBenchmark,
          request_deserializer=logger__pb2.Benchmark.FromString,
          response_serializer=logger__pb2.Empty.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'logger.Logger', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
